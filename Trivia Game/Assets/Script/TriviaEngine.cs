﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(References))]
public class TriviaEngine : MonoBehaviour {

    References refs;
    string right;
    int rounds, yes, no;
    Text numRight, numWrong, numQuestion, textQuestion, buttonOne, buttonTwo, buttonThree,buttonFour;


	
	void Begin () {

        refs = gameObject.GetComponent<References>();
        numRight = GameObject.Find("NumberRight").GetComponent<Text>();
        numWrong = GameObject.Find("NumberWrong").GetComponent<Text>();
        numQuestion = GameObject.Find("QuestionNumber").GetComponent<Text>();
        textQuestion = GameObject.Find("QuestionText").GetComponent<Text>();

        buttonOne = GameObject.Find("AnswerOne").GetComponentInChildren<Text>();
        buttonTwo = GameObject.Find("AnswerTwo").GetComponentInChildren<Text>();
        buttonThree = GameObject.Find("AnswerThree").GetComponentInChildren<Text>();
        buttonFour = GameObject.Find("AnswerFour").GetComponentInChildren<Text>();

        rounds = 1;
       
        numRight.text = "Number Right: 0";
        numWrong.text = "Number Wrong: 0";

        StartRound(rounds);

    }
	
    public void _CheckAnswer(string guess)
    {
        if (guess == right)
        {
            yes++;
            numRight.text = "Number right: " + yes;           
        }
        else
        {
            no++;
            numWrong.text = "Number Wrong: " + no;
        }

        rounds++;
        StartRound(rounds);

    }

    void StartRound(int round)
    {
        numQuestion.text = round.ToString();

        right = refs.correct[round].ToString();    
            
        //display info on buttons
        for (int i = 0; i <10; i++)
        {
            textQuestion.text = refs.questions[i];

            for (int q = 0; q < 4; q++)
            {
                buttonOne.text = refs.answers[q];
                q++;
                buttonTwo.text = refs.answers[q];
                q++;
                buttonThree.text = refs.answers[q];
                q++;
                buttonFour.text = refs.answers[q];             

            }


        }    

    }
}
