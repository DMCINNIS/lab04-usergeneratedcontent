﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class References : MonoBehaviour {

    [HideInInspector]
    public Text numRight, numWrong;
    [HideInInspector]
    public Text questionNumber;
 
    [HideInInspector]
    public List<string> questions = new List<string>();
    [HideInInspector]
    public List<string> answers = new List<string>();
    [HideInInspector]
    public List<string> correct = new List<string>();



    void Load () {

        questions = GetComponent<LoadData>().questions;
        answers = GetComponent<LoadData>().answers;
        correct = GetComponent<LoadData>().correct;

        SendMessage("Begin");
    }
	
	
}
