﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

[RequireComponent(typeof(Resources))]

public class LoadData : MonoBehaviour
{

    FileInfo originalFile; 

    TextAsset textFile;

    TextReader reader;

    //Lists to hold the sentences and clues used in the loop
    //alternating with module 2 ; sentences are even clues odd 
    public List<string> questions = new List<string>();
    public List<string> answers = new List<string>();
    public List<string> correct = new List<string>();

    
    // Use this for initialization
    void Start () {

        originalFile = new FileInfo(Application.dataPath + "/trivia.txt");

        //if that file exists and is not empty
        if (originalFile.Exists && originalFile != null)
        {
            //this is the file we want to open in the reader
            reader = originalFile.OpenText();
        }

        //otherwise we default to the embedded file
        else
        {
            //assigning the object to the raw text in the embedded file,
            //Resoucre.Load() requires you to cast to same type as object
            textFile = (TextAsset)Resources.Load("embedded", typeof(TextAsset));

            //Setup the reader to read the lock and loaded object
            reader = new StringReader(textFile.text);
        }


        //variables to control the logic of reading
        string lineOfText;
        int lineNumber = 0;

        //tell the reader to read a line of text, and store that in the 
        //lineOfTextVariable, continue doing this until there are no lines left
        while ((lineOfText = reader.ReadLine()) != null)
        {
            if (lineNumber % 5 == 0)
            {
                questions.Add(lineOfText);
            }
            else
            {
                answers.Add(lineOfText);
            }

            lineNumber++;

            if (lineNumber % 6 == 0)
            {
                correct.Add(lineOfText);
                lineNumber++;
            }

        }
        SendMessage("Load");
        
    }
}
